//
//  OptionsViewController.swift
//  NoStoryboard4
//
//  Created by Shawn Fernando on 2/2/19.
//  Copyright © 2019 Shawn Fernando. All rights reserved.
//

import Foundation
import UIKit

let yesOrNoAnswerOption = "Yes or No"
let numericalAnswerOption = "Numerical Slider"
let multipleChoiceAnswerOption = "Multiple Choice"
let writtenAnswerOption = "Written"
let audioAnswerOption = "Audio Recording"
let videoAnswerOption = "Video Recording"

//let sliderAnswerOption = "Slider"
var answerTypesArray:[String] = [yesOrNoAnswerOption, numericalAnswerOption, multipleChoiceAnswerOption, writtenAnswerOption, audioAnswerOption, videoAnswerOption]

class CreateQuestionnaireVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    var questionnaireTitle = ""
    let answerTypeLabel = singleColorLabel(UIColor: .gray)
    var questionLabel = singleColorLabel(UIColor: .gray)
//    let yesOrNoAnswerOption = "Yes or No"
//    let numericalAnswerOption = "Numerical"
//    let multipleChoiceAnswerOption = "Multiple Choice"
//    let writtenAnswerOption = "Written"
//    let audioAnswerOption = "Audio Recording"
//    let videoAnswerOption = "Video Recording"
//    let sliderAnswerOption = "Slider"
    //var answerTypesArray:[String] = []
    var questionField =  whiteTextFieldWithPlaceholder(placeholder: "Add a Question...") //
    var answerExampleStack = UIStackView()
    var answerTypeStack = UIStackView()
    var answerTypePicker: UIPickerView!
    var answerTypeButton = buttonWithTitle(title: "Answer Type")
    var mainStack = UIStackView()
    var catcher:String = "Didn't get it"
    var questionTextSaved:String = ""
    var answerOptionSaved:String = ""
    var numericalMinimumSaved:Float = 1
    var numericalMaximumSaved:Float = 10
    var answerOptionsStack = UIStackView()
    var transcriptionEnabledButton = buttonWithTitle(title: "Transcription Enabled") //
    var numericalOptionsStack = UIStackView()
    var numericalFieldButton = buttonWithTitle(title: "Field") //
    var numericalSliderButton = buttonWithTitle(title: "Slider") //
    var numericalSliderStack = UIStackView()
    var numericalSliderVerticalStack = UIStackView()
    var numericalSliderValueLabel = singleColorLabel(UIColor: .gray, text: "Slider Value")
    var numericalSliderValueLabelStack = UIStackView()
    var numericalSliderOptionsStack = UIStackView()
    
    
    var numericalMinimumLabel = singleColorLabel(UIColor: .gray, text: "Min")
    var numericalMaximumLabel = singleColorLabel(UIColor: .gray, text: "Max")
    var numericalMinimumField = whiteTextFieldWithPlaceholder(placeholder: "Min") //
    var numericalMaximumField = whiteTextFieldWithPlaceholder(placeholder: "Max") //
    var multipleChoiceScrollView = UIScrollView()
    var multipleChoiceAddFieldButton = buttonWithTitle(title: "Add Answer Option ➕")
    let audioButton = buttonWithTitle(title: "🎙️") //
    let videoButton = buttonWithTitle(title: "🎥") //
    var writtenOptionTextField = whiteTextFieldWithPlaceholder(placeholder: "Add Question Here") //
    var numericalMinMaxStack = UIStackView()
    var numericalButtonsStack = UIStackView()
    var yesNoStack = UIStackView()
    var numericalSlider = UISlider()
    var questionLabelHeight = NSLayoutConstraint()
    var questionFieldHeight = NSLayoutConstraint()
    var answerTypeLabelHeight = NSLayoutConstraint()
    var answerExampleStackHeight = NSLayoutConstraint()
    var answerExampleStackHeight2 = NSLayoutConstraint()
    var multipleChoiceScrollViewHeight = NSLayoutConstraint()
    var multipleChoiceContentView = UIView()
    var answerTypeStackHeight = NSLayoutConstraint()
    var currentQuestionDict:[String:[String]] = [:]
    var multipleChoiceAnswersArray:[String] = []
    var continueStack = UIStackView()
    var continueStackHeight = NSLayoutConstraint()
    var saveAndContinueButton = buttonWithTitle(title: "Save and Continue ➡️") //
    var saveAndExitButton = buttonWithTitle(title: "⬅️ Save and Exit") //
    var questionNumber:Int = 1
    var answerTypeStackBottom = NSLayoutConstraint()
    
    var answerTypeStackBottom2 = NSLayoutConstraint()
    var continueStackBottom = NSLayoutConstraint()
    var continueStackTop = NSLayoutConstraint()
    var numericalSliderStackLeftBorderWidth = NSLayoutConstraint()
    var numericalSliderStackRightBorderWidth = NSLayoutConstraint()
    var numericalSliderValueStackLeftBorderWidth =  NSLayoutConstraint()
    var numericalSliderValueStackRightBorderWidth = NSLayoutConstraint()
    var numericalSliderStackBottomBorderHeight = NSLayoutConstraint()
    var numericalSliderStackTopBorderHeight = NSLayoutConstraint()
    let numericalSliderStackTopBorder = UIView()
    let numericalSliderStackLeftBorder = UIView()
    let numericalSliderStackRightBorder = UIView()
    let numericalSliderValueStackLeftBorder = UIView()
    let numericalSliderValueStackRightBorder = UIView()
    let numericalSliderStackBottomBorder = UIView()
    
    
    override func viewDidLoad() {
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        mainStack.accessibilityIdentifier = "MainStack"
        situateMainStack(stack: mainStack, reference: self.view)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Exit Without Saving ✖️", style: .plain, target: self, action: #selector(exitTapped))
        saveAndContinueButton.addTarget(self, action: #selector(saveAndContinueTapped(_:)), for: .touchUpInside)
        fillHorizontalStack(elementsArray: [saveAndExitButton, saveAndContinueButton], stack: continueStack)
        continueStack.accessibilityIdentifier = "continueStack"
        mainStack.backgroundColor = .gray //this might not do anything.
        transcriptionEnabledButton.backgroundColor = .green
        self.hideKeyboardWhenTappedAround()
        let yesButton = buttonWithTitle(title: "Yes")
        let noButton = buttonWithTitle(title: "No")
        let tapper = UITapGestureRecognizer(target: self, action:#selector(endEditing(_:)))
        tapper.cancelsTouchesInView = false
        mainStack.addGestureRecognizer(tapper)
        fillHorizontalStack(elementsArray: [yesButton, noButton], stack: yesNoStack)
        yesNoStack.accessibilityIdentifier = "yesNoStack"
        audioButton.isHidden = true
        videoButton.isHidden = true
        writtenOptionTextField.isHidden = true
        //addBorder(view: writtenOptionTextField)
        numericalMinimumLabel.textAlignment = .center
        numericalMaximumLabel.textAlignment = .center
        numericalSliderValueLabel.textAlignment = .center
        numericalSliderValueLabel.font = numericalSliderValueLabel.font.withSize(22.0)
        
        //continueStack.isHidden = true
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: questionField.frame.height))
        
        let numericalSliderValueStackElements = [numericalSliderValueStackLeftBorder, numericalSliderValueLabel, numericalSliderValueStackRightBorder ]
        //let numericalSliderValueStackElements = [numericalSliderValueLabel]
        let numericalSliderStackElements = [numericalSliderStackLeftBorder, numericalMinimumLabel, numericalSlider, numericalMaximumLabel, numericalSliderStackRightBorder]
        //let numericalSliderStackElements = [numericalMinimumLabel, numericalSlider, numericalMaximumLabel]
        
        
        fillHorizontalStackNoWidths(elementsArray: numericalSliderValueStackElements, stack: numericalSliderValueLabelStack)
        numericalSliderValueLabelStack.accessibilityIdentifier = "numericalSliderValueLabelStack"
        
        
        fillHorizontalStackNoWidths(elementsArray: numericalSliderStackElements, stack: numericalSliderStack)
        numericalSliderStack.accessibilityIdentifier = "numericalSliderStack"
        let numericalSliderVerticalStackElements = [numericalSliderStackTopBorder,  numericalSliderValueLabelStack, numericalSliderStack, numericalSliderStackBottomBorder]
        //let numericalSliderVerticalStackElements = [numericalSliderValueLabelStack, numericalSliderStack]
        fillVerticalStackNoHeights(elementsArray: numericalSliderVerticalStackElements, stack: numericalSliderVerticalStack)
        numericalSliderVerticalStack.accessibilityIdentifier = "numericalSliderVerticalStack"
        
        //numericalSliderStackTopBorderHeight.isActive = true
        //numericalSliderStackBottomBorderHeight.isActive = true
        numericalSlider.widthAnchor.constraint(equalTo: numericalMinimumLabel.widthAnchor, multiplier: 8.0).isActive = true
        numericalMaximumLabel.widthAnchor.constraint(equalTo: numericalMinimumLabel.widthAnchor, multiplier: 1.0).isActive = true
        numericalSliderStack.heightAnchor.constraint(equalTo: numericalSliderValueLabel.heightAnchor, multiplier: 3.0).isActive = true
        
        
        let answerExampleStackElements = [yesNoStack, audioButton, videoButton, writtenOptionTextField, numericalSliderVerticalStack, multipleChoiceScrollView]
        
        fillHorizontalStackNoWidths(elementsArray: answerExampleStackElements, stack: answerExampleStack)
        answerExampleStack.accessibilityIdentifier = "answerExampleStack"
        //let answerExampleView = UIView()
        //addSubviewsToReference(subviewArray: answerExampleStackElements, reference: answerExampleView)
        //setViewConstraintsEqualToReference(view: answerExampleView, reference: answerExampleStack)
//        let yesNoStackWidth = yesNoStack.widthAnchor.constraint(equalTo: answerExampleStack.widthAnchor, multiplier: 1.0)
//        //yesNoStackWidth.isActive = true
//        let audioButtonWidth = audioButton.widthAnchor.constraint(equalTo: answerExampleStack.widthAnchor, multiplier: 1.0)
//        let videoButtonWidth = videoButton.widthAnchor.constraint(equalTo: answerExampleStack.widthAnchor, multiplier: 1.0)
//        let writtenOptionTextFieldWidth = writtenOptionTextField.widthAnchor.constraint(equalTo: answerExampleStack.widthAnchor, multiplier: 1.0)
//        let numericalSliderVerticalStackWidth = numericalSliderVerticalStack.widthAnchor.constraint(equalTo: answerExampleStack.widthAnchor, multiplier: 1.0)
//        let multipleChoiceScrollViewWidth = multipleChoiceScrollView.widthAnchor.constraint(equalTo: answerExampleStack.widthAnchor, multiplier: 1.0)
//
        writtenOptionTextField.backgroundColor = .white
        //addBorder(view: writtenOptionTextField)
        writtenOptionTextField.placeholder = "A written answer..."
        answerTypePicker = UIPickerView()
        answerTypePicker.dataSource = self
        answerTypePicker.delegate = self
        answerTypePicker.backgroundColor = .white
        //let questionLabel:UILabel = singleColorLabel(UIColor: .gray)
        
        questionLabel.text = "\(questionnaireTitle): Question #\(questionNumber)"
        questionLabel.font = UIFont.systemFont(ofSize: 25)
        //let answerTypeLabel = singleColorLabel(UIColor: .gray)
        answerTypeLabel.text = "Example Answer"
        answerTypeLabel.textAlignment = .center
        
        numericalOptionsStack.isHidden = true
        numericalFieldButton.setTitle("Field", for: .normal)
        numericalSliderButton.setTitle("Slider", for: .normal)
        numericalFieldButton.backgroundColor = .gray
        numericalSliderButton.backgroundColor = .gray
        numericalMinimumField.keyboardType = .numberPad
        numericalMaximumField.keyboardType = .numberPad
        numericalMinimumField.textAlignment = .center
        numericalMaximumField.textAlignment = .center
        
        numericalMinimumField.addTarget(self, action: #selector(minimumFieldEdited(_:)), for: .editingChanged)
        numericalMaximumField.addTarget(self, action: #selector(maximumFieldEdited(_:)), for: .editingChanged)
        numericalMinimumField.delegate = self
        numericalMaximumField.delegate = self
        
        numericalSlider.backgroundColor = .gray
        numericalSlider.minimumValue = numericalMinimumSaved
        numericalSlider.maximumValue = numericalMaximumSaved
        numericalSliderVerticalStack.isHidden = true
        
        numericalSlider.addTarget(self, action: #selector(sliderValueChanged(_:)), for: .valueChanged)
        //numericalSliderStack.isHidden = true
        fillHorizontalStackNoWidths(elementsArray: [numericalMinimumField, numericalMaximumField], stack: numericalMinMaxStack)
        numericalMinMaxStack.accessibilityIdentifier = "numericalMinMaxStack"
        let numericalMaxWidth = numericalMaximumField.widthAnchor.constraint(equalTo: numericalMinimumField.widthAnchor, multiplier: 1.0)
        numericalMaxWidth.isActive = true
                addSubviewToStack(subview: answerTypePicker, stack: answerTypeStack)
        addSubviewToStack(subview: numericalOptionsStack, stack: answerTypeStack)
        fillHorizontalStack(elementsArray: [numericalFieldButton, numericalSliderButton], stack: numericalButtonsStack)
        numericalButtonsStack.accessibilityIdentifier = "numericalButtonsStack"
        multipleChoiceScrollView.isHidden = true
        
        fillVerticalStack(elementsArray: [numericalMinMaxStack, numericalButtonsStack], stack: numericalOptionsStack)
        numericalOptionsStack.accessibilityIdentifier = "numericalOptionsStack"
        questionField.leftView = paddingView
        questionField.leftViewMode = .always
        questionField.addTarget(self, action: #selector(questionFieldEdited(_:)), for: .editingDidEnd)
        questionField.addTarget(self, action: #selector(questionFieldTapped(_:)), for: .editingDidBegin)
        
        answerTypeButton.addTarget(self, action: #selector(answerTypeButtonPressed(_:)), for: .touchUpInside)
        let mainStackElementsArray : [UIView] = [questionLabel, questionField, answerTypeLabel, answerExampleStack, answerTypeStack, continueStack]
        fillVerticalStackNoHeightsNoBottom(elementsArray: mainStackElementsArray, stack: mainStack)
        continueStack.isHidden = true
        answerTypeStackBottom = answerTypeStack.bottomAnchor.constraint(equalTo: mainStack.bottomAnchor)
        answerTypeStackBottom.isActive = true
        answerTypeStackBottom2 = answerTypeStack.bottomAnchor.constraint(equalTo: continueStack.topAnchor)
        continueStackBottom = continueStack.bottomAnchor.constraint(equalTo: mainStack.bottomAnchor)
        continueStackTop = continueStack.topAnchor.constraint(equalTo: answerTypeStack.bottomAnchor)
        setConstraintVariables()
        addBorder(view: videoButton)
        addBorder(view: audioButton)
        addBorder(view: saveAndExitButton)
        addBorder(view: saveAndContinueButton)
        addBorder(view: questionField)
        addBorder(view: numericalSliderButton)
        addBorder(view: numericalFieldButton)
        addBorder(view: answerTypePicker)
        addBorder(view: questionLabel, cornerRadius:0.0)
        addBorder(view: answerTypeLabel, cornerRadius:0.0)
        addBorder(view: yesButton)
        addBorder(view: noButton)
        addBorder(view: numericalMinimumField)
        addBorder(view: numericalMaximumField)
        
        //numericalSliderVerticalStack.layer.borderWidth = 2.0
        //numericalSliderVerticalStack.layer.borderColor = (UIColor.black).cgColor

        //print(mainStack.subviews)
    }
    
    func showContinueStack(){
        
        answerTypeStackBottom.isActive = false
        answerTypeStackBottom2.isActive = true
        //continueStackBottom.isActive = true
        //continueStackTop.isActive = true
        continueStack.isHidden = false
    }
    
    func hideContinueStack(){
        //continueStack.isHidden = true
        //answerTypeStackBottom.isActive = true
        //answerTypeStackBottom2.isActive = false
        continueStack.isHidden = true
    }
    
    func setConstraintVariables(){
        questionFieldHeight = questionField.heightAnchor.constraint(equalTo: questionLabel.heightAnchor, multiplier: 2.0)
        questionFieldHeight.isActive = true
        answerTypeLabelHeight = answerTypeLabel.heightAnchor.constraint(equalTo: questionLabel.heightAnchor, multiplier: 0.6)
        answerTypeLabelHeight.isActive = true
        answerExampleStackHeight = answerExampleStack.heightAnchor.constraint(equalTo: questionLabel.heightAnchor, multiplier: 2.0)
        answerExampleStackHeight2 = answerExampleStack.heightAnchor.constraint(equalTo: questionLabel.heightAnchor, multiplier: 4.0)
        answerExampleStackHeight.isActive = true
        answerTypeStackHeight = answerTypeStack.heightAnchor.constraint(equalTo: questionLabel.heightAnchor, multiplier: 3.0)
        answerTypeStackHeight.isActive = true
        continueStackHeight = continueStack.heightAnchor.constraint(equalTo: questionLabel.heightAnchor, multiplier: 1.0)
        continueStackHeight.isActive = true
    }
    
    
    @objc func answerTypeButtonPressed(_ sender:UIButton){
        print("hit it")
        let vc = AnswerTypeVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func questionFieldEdited(_ sender:UITextField){
        print("field edited")
        if questionField.text != ""{
            //self.questionField.layer.cornerRadius = 0.0
            self.questionField.backgroundColor = .lightGray
            //self.questionField.layer.borderWidth = 0.0
            self.questionField.textAlignment = .center
            self.dismissKeyboard()
            showContinueStack()
        }
        if questionField.text == ""{
            hideContinueStack()
            //self.continueStack.isHidden = true
        }
        //self.questionField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        //self.questionField.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        //let vc = AnswerTypeVC()
        //self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func questionFieldTapped(_ sender:UITextField){
        print("field pressed")
        //self.questionField.layer.cornerRadius = 10.0
        self.questionField.backgroundColor = .white
        //self.questionField.layer.borderWidth = 2.0
        self.questionField.textAlignment = .left
        //self.questionField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        //self.questionField.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        //let vc = AnswerTypeVC()
        //self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getCurrentViewControllerIndex(){
        
    }
    
    @objc func exitTapped(_ sender:UIBarButtonItem){
        print("Exit Tapped")
        //let viewControllersCount = self.navigationController?.viewControllers.count
        //let highestIndex = viewControllersCount! - 1
        navigationController?.popToRootViewController(animated: false)
        
        //self.navigationController?.popToViewController((self.navigationController?.viewControllers[0])!, animated: true)
//        let controllerCount = self.navigationController?.viewControllers.count ?? 0
//        //print(controllerCount)
//        var currentVCIndex:Int = 0
//        for index in 0...(controllerCount - 1){
//            if self.navigationController?.viewControllers[index] == self{
//            print ("Found self index")
//            print(index)
//            currentVCIndex = index
//            }
//        }
//        print(currentVCIndex)
//        print(controllerCount)
//        let minusOne = (controllerCount - 1)
//        if currentVCIndex < minusOne{
//            print("there are more items on the stack")
//            let destinationVC = self.navigationController?.viewControllers[currentVCIndex+1]
//            self.navigationController?.pushViewController(destinationVC!, animated: true)
//        }
        
        
        
        //        print ("Bar Button Tapped")
    }
    @objc func saveAndContinueTapped(_ sender:UIButton){
        
        print(self.numericalMinimumField.text as Any)
        print(self.numericalMaximumField.text as Any)
        currentQuestionDict["Type"] = [answerTypesArray[self.answerTypePicker.selectedRow(inComponent: 0)]]
        currentQuestionDict["Question"] = [self.questionField.text] as? [String]
        currentQuestionDict["Min"] = [self.numericalMinimumField.text] as? [String]
        currentQuestionDict["Max"] = [self.numericalMaximumField.text] as? [String]
        currentQuestionDict["Multiple Choice Answers"] = multipleChoiceAnswersArray
        currentQuestionDict["Question Number"] = [String(questionNumber)]
        //print(currentQuestionDict)
        let type = answerTypesArray[self.answerTypePicker.selectedRow(inComponent: 0)]
        let question = self.questionField.text
        let min = Int(self.numericalMinimumField.text ?? "1")
        let max = Int(self.numericalMaximumField.text ?? "10")
        
        let currentQuestion = Question(type: type, question: question, min: min, max: max, multipleChoiceAnswers: multipleChoiceAnswersArray, questionNumber: questionNumber)
        
        if validateQuestion(question: currentQuestion) == false{
            return
        }
        
        
        
//        else{
//            let newQuestionnaire = [String(questionNumber):[("someString", "someOther")]]
//            UserDefaults.standard.set(newQuestionnaire, forKey: questionnaireTitle)
//            print ("newQuestionnaire \(newQuestionnaire as Any)")
//            let retrievedQ = UserDefaults.standard.object(forKey: questionnaireTitle) as? [String:[(String)]]
//            print ("retrievedQ \(retrievedQ as Any)")
//
//        }
        
        print("heading to next Question")
        print(currentQuestion as Any)
        let existingQuestionnaire = UserDefaults.standard.object(forKey: questionnaireTitle)
        print(existingQuestionnaire as Any)
        //UserDefaults.standard.set([currentQuestion] as [Question], forKey: questionnaireTitle)
        let vc = CreateQuestionnaireVC()
        vc.questionNumber = self.questionNumber + 1
        vc.questionnaireTitle = self.questionnaireTitle
        print (self.navigationController as Any)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func resetAnswerExampleField(){
        self.hideSlider()
        self.hideMultipleChoice()
        self.numericalOptionsStack.isHidden = true
        self.answerExampleStackHeight.isActive = true
        self.answerExampleStackHeight2.isActive = false
        for subview in self.answerExampleStack.subviews{
            if subview.isHidden == false{
                subview.isHidden = true
            }
        }
        
    }
    
    func createMultipleChoiceHeightConstraints(viewArray:[UIView]) -> [NSLayoutConstraint]{
        var constraintArray:[NSLayoutConstraint] = []
        for view in viewArray{
            let newConstraint = view.heightAnchor.constraint(equalToConstant: 100)
            newConstraint.isActive = true
            constraintArray.append(newConstraint)
        }
        return constraintArray
    }
    
    func showMultipleChoice(){
        self.multipleChoiceScrollView.isHidden = false
        self.multipleChoiceScrollView.backgroundColor = .gray
        let textFieldTest = whiteTextFieldWithPlaceholder(placeholder: "Enter A Multiple Choice Option Here.")
        fillVerticalScrollViewNoHeights(container: self.answerExampleStack, scroll: self.multipleChoiceScrollView, elementsArray: [textFieldTest])
        textFieldTest.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    func hideMultipleChoice(){
        self.multipleChoiceScrollView.isHidden = false
        
        
    }
    
    func showSlider(){
        numericalSliderStackLeftBorderWidth = numericalSliderStackLeftBorder.widthAnchor.constraint(equalToConstant: 2.0)
        numericalSliderStackLeftBorderWidth.isActive = true
        numericalSliderStackRightBorderWidth = numericalSliderStackRightBorder.widthAnchor.constraint(equalToConstant: 2.0)
        numericalSliderStackRightBorderWidth.isActive = true
        numericalSliderValueStackLeftBorderWidth =  numericalSliderValueStackLeftBorder.widthAnchor.constraint(equalToConstant: 2.0)
        numericalSliderValueStackLeftBorderWidth.isActive = true
        numericalSliderValueStackRightBorderWidth = numericalSliderValueStackRightBorder.widthAnchor.constraint(equalToConstant: 2.0)
        numericalSliderValueStackRightBorderWidth.isActive = true
        numericalSliderStackBottomBorderHeight = numericalSliderStackBottomBorder.heightAnchor.constraint(equalToConstant: 2.0)
        numericalSliderStackBottomBorderHeight.isActive = true
        numericalSliderStackTopBorderHeight = numericalSliderStackTopBorder.heightAnchor.constraint(equalToConstant: 2.0)
        numericalSliderStackTopBorderHeight.isActive = true
    }
    
    func hideSlider(){
        numericalSliderStackLeftBorderWidth.isActive = false
        numericalSliderStackRightBorderWidth.isActive = false
        numericalSliderValueStackLeftBorderWidth.isActive = false
        numericalSliderValueStackRightBorderWidth.isActive = false
        numericalSliderStackBottomBorderHeight.isActive = false
        numericalSliderStackTopBorderHeight.isActive = false
    }
    
    func validateMinMax() -> Bool{
        let min = self.numericalMinimumField.text ?? ""
        let max = self.numericalMaximumField.text ?? ""
        print("MinMax \(min, max)")
        return true
    }
    
    
        
        //        if min == "" || max != ""{
//            print("empty String")
//            return false
//        }
//
//        let intMin = Int(min)
//        let intMax = Int(max)
//        if intMin != nil && intMax != nil{
//            if intMin! < intMax!{
//                return true
//            }
//            else {
//                return false}
//        }
//
//        else{
//            print("a value must have been nil")
//            return false}
        
        
    @objc func endEditing(_ sender:UIGestureRecognizer){
        questionField.endEditing(true)
    }
    
    @objc func backButtonTapped(_ sender:UIButton){
        print("backButtonTapped")
        let vc = CreateQuestionnaireVC()
        vc.questionNumber = self.questionNumber + 1
        print (self.navigationController as Any)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @objc func minimumFieldEdited(_ sender:UITextField){
        let minimum = self.numericalMinimumField.text
        let minimumInt = Int(minimum ?? "0")
        if minimum == nil{
            print("minimum is nil")
        }
        self.numericalSlider.minimumValue = Float(minimumInt ?? 0)
        numericalMinimumLabel.text = minimum
    }
    
    @objc func maximumFieldEdited(_ sender:UITextField){
        let maximum = self.numericalMaximumField.text
        let maximumInt = Int(maximum ?? "0")
        if maximum == nil{
            print("maximum is nil")
        }
        self.numericalSlider.maximumValue = Float(maximumInt ?? 0)
        numericalMaximumLabel.text = maximum
    }

    @objc func sliderValueChanged(_ sender:UISlider){
        let currentValue = Int(sender.value)
        let stringValue = String(currentValue)
        sender.value = Float(currentValue)
        numericalSliderValueLabel.text = stringValue
        if numericalMaximumField.text == ""{
            numericalMaximumLabel.text = String(Int(numericalSlider.maximumValue))
        }
        if numericalMinimumField.text == ""{
            numericalMinimumLabel.text = String(Int(numericalSlider.minimumValue))
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters) == nil
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return answerTypesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        //print("\(#function) fired")
        
        return answerTypesArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        answerTypeLabel.text = answerTypesArray[row]
        if answerTypeLabel.text == multipleChoiceAnswerOption{
             print("hello")
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                self.hideSlider()
                self.numericalOptionsStack.isHidden = true
                self.answerExampleStackHeight.isActive = true
                self.answerExampleStackHeight2.isActive = false
                for subview in self.answerExampleStack.subviews{
                    if subview.isHidden == false{
                        subview.isHidden = true
                    }
                }
                
            }
                , completion: {(Bool)->Void in
                    UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in

                        self.answerExampleStackHeight.isActive = false
                        self.answerExampleStackHeight2.isActive = true
                        self.showMultipleChoice()
                        //addBorder(view: textFieldTest)
                        
                        //self.multipleChoiceScrollView.backgroundColor = .gray
                        //self.multipleChoiceScrollView.isHidden = false
//                        self.answerExampleStack.removeConstraints(self.answerExampleStack.constraints)
//                        self.answerExampleStack.heightAnchor.constraint(equalTo: self.questionLabel.heightAnchor, multiplier: 6.0).isActive = true
//                            self.view.layoutIfNeeded()
                        //self.answerExampleStack.heightAnchor.constraint(equalTo: self.questionLabel.heightAnchor, multiplier: 6.0).isActive = true
                        
                    }
                        , completion: {(Bool) -> Void in
                            self.answerExampleStack.shake()})
                    
                    
            })
            
        }
        if answerTypeLabel.text == videoAnswerOption{
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                self.numericalOptionsStack.isHidden = true
                self.hideSlider()
                self.answerExampleStackHeight.isActive = true
                self.answerExampleStackHeight2.isActive = false

                for subview in self.answerExampleStack.subviews{
                    if subview.isHidden == false{
                        subview.isHidden = true
                    }
                }
                
            }
                , completion: {(Bool)->Void in
                    UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in                        
                       
                        self.videoButton.isHidden = false
                        
                    }
                        , completion: nil)
                    
                    
            })
            
        }
        if answerTypeLabel.text == numericalAnswerOption{
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: UIView.AnimationOptions(), animations: {()->Void in
                
                self.answerExampleStackHeight.isActive = true
                self.answerExampleStackHeight2.isActive = false
                
                self.numericalOptionsStack.isHidden = false
                setViewWidthToReference(view: self.numericalOptionsStack, reference: self.answerTypePicker.widthAnchor, multiplier: 0.5)
        
            }
                , completion: nil)
        
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                
                self.answerExampleStackHeight.isActive = true
                self.answerExampleStackHeight2.isActive = false
                for subview in self.answerExampleStack.subviews{
                    if subview.isHidden == false{
                        subview.isHidden = true
                    }
                }
                
            }
                , completion: {(Bool)->Void in
                    UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                        
                        self.showSlider()
                        self.numericalSliderVerticalStack.isHidden = false
                        
                        //self.writtenOptionTextField.isHidden = false
                        
                    }
                        , completion: nil)
                    
                    
            })
            
            }
        
        if answerTypeLabel.text == writtenAnswerOption{
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                self.hideSlider()
                self.numericalOptionsStack.isHidden = true
                self.answerExampleStackHeight.isActive = true
                self.answerExampleStackHeight2.isActive = false
                for subview in self.answerExampleStack.subviews{
                    if subview.isHidden == false{
                        subview.isHidden = true
                    }
                }
                
            }
                , completion: {(Bool)->Void in
                    UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                        
                        self.numericalOptionsStack.isHidden = true
                        self.writtenOptionTextField.isHidden = false
                        
                    }
                        , completion: nil)
                    
                    
            })
            
        }
        if answerTypeLabel.text == audioAnswerOption{
        
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                self.resetAnswerExampleField()
                for subview in self.answerExampleStack.subviews{
                    if subview.isHidden == false{
                        subview.isHidden = true
                    }
                }
                
                }
                , completion: {(Bool)->Void in
                    UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                        
                        self.numericalOptionsStack.isHidden = true
                        self.audioButton.isHidden = false
                        
                    }
                        , completion: nil)
                    
            
            })
            
        }
        if answerTypeLabel.text == yesOrNoAnswerOption{
            
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                self.hideSlider()
                self.numericalOptionsStack.isHidden = true
                self.answerExampleStackHeight.isActive = true
                self.answerExampleStackHeight2.isActive = false
                for subview in self.answerExampleStack.subviews{
                    if subview.isHidden == false{
                        subview.isHidden = true
                    }
                }
                
            }
                , completion: {(Bool)->Void in
                    UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {()->Void in
                        
                        self.numericalOptionsStack.isHidden = true
                        self.yesNoStack.isHidden = false
                        
                    }
                        , completion: nil)
                    
                    
            })
            
        }
        
    }

}

//UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: UIView.AnimationOptions(), animations: {()->Void in
//
//
//    self.yesNoStack.isHidden = false
//
//}
//    , completion: nil)
//
//}
