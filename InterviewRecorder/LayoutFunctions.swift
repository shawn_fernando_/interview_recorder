//
//  LayoutFunc.swift
//  NoStoryboard4
//
//  Created by Shawn Fernando on 1/24/19.
//  Copyright © 2019 Shawn Fernando. All rights reserved.
//

import Foundation
import UIKit


//
//func animateHideWithSpring(withDuration:CGFloat  = 1.0, delay:CGFloat = 0.0, usingSpringWithDamping:CGFloat = 0.8, initialSpringVelocity:CGFloat = 1.0, completion:@escaping(Bool)->Void){
//    UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIView.AnimationOptions(), animations: {(()->Void).self
//    }
//        , completion: nil)
//}

func hideViewsInArray(viewsArray:[UIView]){
    for item in viewsArray{
        item.isHidden = true
    }
}

func setAnchorsToLayoutGuide(view:UIView, guide:UILayoutGuide){
    setViewTopToReference(view: view, reference: guide.topAnchor)
    setViewBottomToReference(view: view, reference: guide.bottomAnchor)
    setViewLeadingToReference(view: view, reference: guide.leadingAnchor)
    setViewTrailingToReference(view: view, reference: guide.trailingAnchor)
    
}

func fillHorizontalStackNoWidths(elementsArray:[UIView], stack:UIStackView, proportionsArray:[Float] = []){
    stack.axis = .horizontal
    if elementsArray.isEmpty{
        return
    }
    let lastIndex = elementsArray.count - 1
    for item in elementsArray{
        addSubviewToStack(subview: item, stack: stack)
        useAutoLayoutForView(view: item)
        //print (stack.arrangedSubviews)
    }
    setViewLeadingToReference(view: elementsArray[0], reference: stack.leadingAnchor)
    setViewTrailingToReference(view: elementsArray[lastIndex], reference: stack.trailingAnchor)
    for item in elementsArray{
        //addSubviewToReference(subview: item, reference: stack)
        setViewTopToReference(view: item, reference: stack.topAnchor)
        setViewBottomToReference(view: item, reference: stack.bottomAnchor)
    }
    if elementsArray.count == 1{
        print ("this array only has one element")
        return
    }
    for index in 1...lastIndex{
        setViewLeadingToReference(view: elementsArray[index], reference: elementsArray[index-1].trailingAnchor)
    }
    return
    
}

func situateMainStack(stack: UIStackView, reference:UIView){
    //stack.distribution = .fillProportionally
    addSubviewToReference(subview: stack, reference: reference)
    useAutoLayoutForView(view: stack)
    let safeArea = reference.safeAreaLayoutGuide
    setAnchorsToLayoutGuide(view: stack, guide: safeArea)
}

func fillVerticalStackNoHeights(elementsArray:[UIView], stack:UIStackView){
    stack.axis = .vertical
    if elementsArray.isEmpty{
        return
    }
    let lastIndex = elementsArray.count - 1
    for item in elementsArray{
        addSubviewToStack(subview: item, stack: stack)
        useAutoLayoutForView(view: item)
    }
    setViewTopToReference(view: elementsArray[0], reference: stack.topAnchor)
    setViewBottomToReference(view: elementsArray[lastIndex], reference: stack.bottomAnchor)
    for item in elementsArray{
        //addSubviewToReference(subview: item, reference: stack)
        setViewLeadingToReference(view: item, reference: stack.leadingAnchor)
        setViewTrailingToReference(view: item, reference: stack.trailingAnchor)
    }
    if elementsArray.count == 1{
        print ("this array only has one element")
        return
    }
    for index in 1...lastIndex{
        setViewTopToReference(view: elementsArray[index], reference: elementsArray[index-1].bottomAnchor)
    }
    return
    
}

func fillVerticalStackNoHeightsNoBottom(elementsArray:[UIView], stack:UIStackView){
    stack.axis = .vertical
    if elementsArray.isEmpty{
        return
    }
    let lastIndex = elementsArray.count - 1
    for item in elementsArray{
        addSubviewToStack(subview: item, stack: stack)
        useAutoLayoutForView(view: item)
    }
    setViewTopToReference(view: elementsArray[0], reference: stack.topAnchor)
    //setViewBottomToReference(view: elementsArray[lastIndex], reference: stack.bottomAnchor)
    for item in elementsArray{
        //addSubviewToReference(subview: item, reference: stack)
        setViewLeadingToReference(view: item, reference: stack.leadingAnchor)
        setViewTrailingToReference(view: item, reference: stack.trailingAnchor)
    }
    if elementsArray.count == 1{
        print ("this array only has one element")
        return
    }
    for index in 1...lastIndex{
        setViewTopToReference(view: elementsArray[index], reference: elementsArray[index-1].bottomAnchor)
    }
    return
    
}


func fillVerticalStack(elementsArray:[UIView], stack:UIStackView, proportionsArray:[Float] = []){
    stack.axis = .vertical
    
    if elementsArray.isEmpty{
        return
    }
    let lastIndex = elementsArray.count - 1
    for item in elementsArray{
        addSubviewToStack(subview: item, stack: stack)
        useAutoLayoutForView(view: item)
    }
    setViewTopToReference(view: elementsArray[0], reference: stack.topAnchor)
    setViewBottomToReference(view: elementsArray[lastIndex], reference: stack.bottomAnchor)
    for item in elementsArray{
        //addSubviewToReference(subview: item, reference: stack)
        setViewLeadingToReference(view: item, reference: stack.leadingAnchor)
        setViewTrailingToReference(view: item, reference: stack.trailingAnchor)
    }
    if elementsArray.count == 1{
        print ("this array only has one element")
        return
    }
    for index in 1...lastIndex{
        if elementsArray.dropFirst().count == proportionsArray.count{
            setViewHeightToReference(view: elementsArray[index], reference: elementsArray[0].heightAnchor, multiplier: proportionsArray[index-1])
            
        }
        else {
        setViewHeightToReference(view: elementsArray[index], reference: elementsArray[0].heightAnchor)
        }
        
        setViewTopToReference(view: elementsArray[index], reference: elementsArray[index-1].bottomAnchor)
    }
    return
    
}

func fillVerticalStackNew(elementsArray:[UIView], stack:UIStackView, proportionsArray:[Float] = []){
    stack.axis = .vertical
    
    if elementsArray.isEmpty{
        return
    }
    let lastIndex = elementsArray.count - 1
    for item in elementsArray{
        addSubviewToStack(subview: item, stack: stack)
        useAutoLayoutForView(view: item)
    }
    setViewTopToReference(view: elementsArray[0], reference: stack.topAnchor)
    setViewBottomToReference(view: elementsArray[lastIndex], reference: stack.bottomAnchor)
    for item in elementsArray{
        //addSubviewToReference(subview: item, reference: stack)
        setViewLeadingToReference(view: item, reference: stack.leadingAnchor)
        setViewTrailingToReference(view: item, reference: stack.trailingAnchor)
    }
    if elementsArray.count == 1{
        print ("this array only has one element")
        return
    }
    for index in 1...lastIndex{
        if elementsArray.dropFirst().count == proportionsArray.count{
            setViewHeightToReference(view: elementsArray[index], reference: elementsArray[0].heightAnchor, multiplier: proportionsArray[index-1])
            
        }
        else {
            setViewHeightToReference(view: elementsArray[index], reference: elementsArray[0].heightAnchor)
        }
        
        setViewTopToReference(view: elementsArray[index], reference: elementsArray[index-1].bottomAnchor)
    }
    return
    
}


func fillHorizontalStack(elementsArray:[UIView], stack:UIStackView, proportionsArray:[Float] = []){
    stack.axis = .horizontal
    if elementsArray.isEmpty{
        return
    }
    let lastIndex = elementsArray.count - 1
    for item in elementsArray{
        addSubviewToStack(subview: item, stack: stack)
        useAutoLayoutForView(view: item)
        //print (stack.arrangedSubviews)
    }
    setViewLeadingToReference(view: elementsArray[0], reference: stack.leadingAnchor)
    setViewTrailingToReference(view: elementsArray[lastIndex], reference: stack.trailingAnchor)
    for item in elementsArray{
        //addSubviewToReference(subview: item, reference: stack)
        setViewTopToReference(view: item, reference: stack.topAnchor)
        setViewBottomToReference(view: item, reference: stack.bottomAnchor)
    }
    if elementsArray.count == 1{
        print ("this array only has one element")
        return
    }
    for index in 1...lastIndex{
        if elementsArray.dropFirst().count == proportionsArray.count{
            setViewWidthToReference(view: elementsArray[index], reference: elementsArray[0].widthAnchor, multiplier: proportionsArray[index-1])
            
        }
        else {
            setViewWidthToReference(view: elementsArray[index], reference: elementsArray[0].widthAnchor)
        }
        
        setViewLeadingToReference(view: elementsArray[index], reference: elementsArray[index-1].trailingAnchor)
    }
    return
    
}

func addBorder(view:UIView, borderWidth:CGFloat = 2.0, cornerRadius: CGFloat = 10.0){
    view.layer.borderWidth = borderWidth
    view.layer.cornerRadius = cornerRadius
    
}

func addSubviewToStack(subview: UIView, stack: UIStackView){
    stack.addArrangedSubview(subview)
}

func addSubviewToReference(subview:UIView, reference:UIView){
    reference.addSubview(subview)

}

func addSubviewsToReference(subviewArray:[UIView], reference:UIView){
    for subview in subviewArray{
        reference.addSubview(subview)
    }
}

func useAutoLayoutForView(view: UIView){
    view.translatesAutoresizingMaskIntoConstraints = false
}

func setViewTopToReference(view: UIView, reference: NSLayoutYAxisAnchor, constant:CGFloat = 0){
    view.topAnchor.constraint(equalTo:reference, constant: constant).isActive = true
}

func setViewBottomToReference(view: UIView, reference: NSLayoutYAxisAnchor, constant:CGFloat = 0){
    view.bottomAnchor.constraint(equalTo:reference, constant: -constant).isActive = true
}


func setViewLeftToReference(view: UIView, reference: NSLayoutXAxisAnchor, constant:CGFloat = 0){
    view.leftAnchor.constraint(equalTo:reference, constant: constant).isActive = true
}

func setViewRightToReference(view: UIView, reference: NSLayoutXAxisAnchor, constant:CGFloat = 0){
    view.rightAnchor.constraint(equalTo:reference, constant: -constant).isActive = true
}

func setViewLeadingToReference(view: UIView, reference: NSLayoutXAxisAnchor, constant:CGFloat = 0){
    view.leadingAnchor.constraint(equalTo:reference, constant: constant).isActive = true
}

func setViewTrailingToReference(view: UIView, reference: NSLayoutXAxisAnchor, constant:CGFloat = 0){
    view.trailingAnchor.constraint(equalTo:reference, constant: -constant).isActive = true
}

func setViewHeightToReference(view: UIView, reference:NSLayoutDimension, multiplier: Float = 1.0){
    view.heightAnchor.constraint(equalTo: reference, multiplier: CGFloat(multiplier)).isActive = true
}

func setViewWidthToReference(view: UIView, reference:NSLayoutDimension, multiplier: Float = 1.0){
    view.widthAnchor.constraint(equalTo: reference, multiplier: CGFloat(multiplier)).isActive = true
}

func setViewConstraintsEqualToReference(view:UIView, reference:UIView){
    setViewTopToReference(view: view, reference: reference.topAnchor)
    setViewBottomToReference(view: view, reference: reference.bottomAnchor)
    setViewLeadingToReference(view: view, reference: reference.leadingAnchor)
    setViewTrailingToReference(view: view, reference: reference.trailingAnchor)
}

func fillVerticalScrollViewNoHeights(container:UIView, scroll: UIScrollView, elementsArray:[UIView]){
    if elementsArray.isEmpty{
        return
        }
    
    container.translatesAutoresizingMaskIntoConstraints = false
    scroll.translatesAutoresizingMaskIntoConstraints = false
    
    container.backgroundColor = .purple
    scroll.backgroundColor = .gray
    container.addSubview(scroll)
    
    setViewTopToReference(view: scroll, reference: container.topAnchor)
    setViewLeadingToReference(view: scroll, reference: container.leadingAnchor)
    setViewWidthToReference(view: scroll, reference: container.widthAnchor)
    setViewHeightToReference(view: scroll, reference: container.heightAnchor)
    
    
    let lastIndex = elementsArray.count - 1
    for item in elementsArray{
        addSubviewToReference(subview: item, reference: scroll)
        item.translatesAutoresizingMaskIntoConstraints = false
        }
    setViewTopToReference(view: elementsArray[0], reference: scroll.topAnchor)
    setViewBottomToReference(view: elementsArray[lastIndex], reference: scroll.bottomAnchor)
    for item in elementsArray{
        //addSubviewToReference(subview: item, reference: stack)
        setViewLeadingToReference(view: item, reference: scroll.leadingAnchor)
        setViewWidthToReference(view: item, reference: container.widthAnchor)
        }
    if elementsArray.count == 1{
        print ("this array only has one element")
        return
        }
    for index in 1...lastIndex{
        setViewTopToReference(view: elementsArray[index], reference: elementsArray[index-1].bottomAnchor)
        }
    return
    }

    func createButtonArrayFromStringArray(stringArray:[String]) -> [UIButton]{
        var returnArray:[UIButton] = []
        for itemString in stringArray{
            let newButton = buttonWithTitle(title: itemString)
            newButton.backgroundColor = .gray
            returnArray.append(newButton)
        }
        return returnArray
    }

//
//    testLabel.translatesAutoresizingMaskIntoConstraints = false
//    addSubviewToReference(subview: testLabel, reference: scroll)
//
//    setViewTopToReference(view: testLabel, reference: scroll.topAnchor)
//    setViewLeadingToReference(view: testLabel, reference: scroll.leadingAnchor)
//    setViewWidthToReference(view: testLabel, reference: container.widthAnchor)
//    testLabel.heightAnchor.constraint(equalToConstant: 1000).isActive = true
//
//    testLabel2.translatesAutoresizingMaskIntoConstraints = false
//    addSubviewToReference(subview: testLabel2, reference: scroll)
//
//    setViewTopToReference(view: testLabel2, reference: testLabel.bottomAnchor)
//    setViewLeadingToReference(view: testLabel, reference: scroll.leadingAnchor)
//    setViewWidthToReference(view: testLabel, reference: container.widthAnchor)
//    testLabel2.heightAnchor.constraint(equalToConstant: 1000).isActive = true
//
//
//    setViewBottomToReference(view: testLabel2, reference: scroll.bottomAnchor)

