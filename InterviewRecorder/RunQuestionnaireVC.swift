//
//  ConductQuestionnaireVC.swift
//  InterviewRecorder
//
//  Created by Shawn Fernando on 2/15/19.
//  Copyright © 2019 SunnyBear Software. All rights reserved.
//
import Foundation
import UIKit

class RunQuestionnaireVC: UIViewController{
    var mainStack = UIStackView()
    var catcher:String = "Didn't get it"
    var questionLabel = grayLabelWithStraightBorderAndTitle(title: "")
    var writtenField = UITextField()
    var multipleChoiceStack = UIStackView()
    var answerStack = UIStackView()
    var yesNoStack = UIStackView()
    var yesButton = grayButtonWithBorderAndTitle(title: "Yes")
    var noButton = grayButtonWithBorderAndTitle(title: "No")
    var slider = UISlider()
    var exampleQuestionArray:[String:[String]] = ["Question": ["This is a Question"], "Min": [""], "Type": [yesOrNoAnswerOption], "Max": [""], "Multiple Choice Answers": []]
    var answerStackElements:[UIView] = []
    
    override func viewDidLoad() {
        questionLabel.text = exampleQuestionArray["Question"]?[0] ?? ""
        questionLabel.backgroundColor = .gray
        writtenField.backgroundColor = .white
        yesButton.backgroundColor = .gray
        noButton.backgroundColor = .gray
        fillHorizontalStack(elementsArray: [yesButton, noButton], stack: yesNoStack)
        answerStackElements = [writtenField, slider, yesNoStack]
        fillHorizontalStack(elementsArray: answerStackElements, stack: answerStack, proportionsArray: [1.0])
        hideViewsInArray(viewsArray:answerStackElements)
        fillVerticalStack(elementsArray: [questionLabel, answerStack], stack: mainStack, proportionsArray: [1.0])
        makeQuestionView(questionArray: exampleQuestionArray)
        mainStack.distribution = .fillProportionally
        addSubviewToReference(subview: mainStack, reference: self.view)
        useAutoLayoutForView(view: mainStack)
        let safeArea = view.safeAreaLayoutGuide
        setAnchorsToLayoutGuide(view: mainStack, guide: safeArea)
        
    }
    
    func makeQuestionView(questionArray:[String:Any]){
        if exampleQuestionArray["Question"] == nil{
            print("Something Terrible has happened")
            return
        }
        questionLabel.text = exampleQuestionArray["Question"]?[0] ?? ""
        print(exampleQuestionArray["Type"] as Any)
        if exampleQuestionArray["Type"] == [writtenAnswerOption]{
            hideViewsInArray(viewsArray:answerStackElements)
            self.writtenField.isHidden = false
            view.layoutIfNeeded()
        }
        if exampleQuestionArray["Type"] == [numericalAnswerOption]{
            hideViewsInArray(viewsArray:answerStackElements)
            self.slider.isHidden = false
            view.layoutIfNeeded()
        }
        if exampleQuestionArray["Type"] == [yesOrNoAnswerOption]{
            hideViewsInArray(viewsArray:answerStackElements)
            yesNoStack.isHidden = false
        }
        if exampleQuestionArray["Type"] == [multipleChoiceAnswerOption]{
            hideViewsInArray(viewsArray:answerStackElements)
        }
        if exampleQuestionArray["Type"] == [audioAnswerOption]{
            hideViewsInArray(viewsArray:answerStackElements)
        }
        if exampleQuestionArray["Type"] == [videoAnswerOption]{
            hideViewsInArray(viewsArray:answerStackElements)
        }

    }
}
//    @objc func playGameTapped(_ sender:UIButton){
//        print("did the thing")
//        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 10.0, options: UIView.AnimationOptions(), animations: {()->Void in
//
//
//                let newPlayGameButtonWidth = self.playGameButton.widthAnchor.constraint(equalTo: self.gameSettingsButton.widthAnchor, multiplier: 2.0)
//
//            self.playGameButtonWidth.isActive = false
//            newPlayGameButtonWidth.isActive = true
//            }
//                , completion: nil)
//
//        playGameButton.shake()
//    }
//    @objc func gameSettingsTapped(_ sender:UIButton){
//        print("did the thing")
//        self.playGameButtonWidth = self.playGameButton.widthAnchor.constraint(equalTo: gameSettingsButton.widthAnchor, multiplier: 1.0)
//        view.layoutIfNeeded()
//    }

extension UIView {
    func shake() {
//        let animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
//        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
//        animation.duration = 0.6
//        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
//        layer.add(animation, forKey: "shake")
    }
   

}

