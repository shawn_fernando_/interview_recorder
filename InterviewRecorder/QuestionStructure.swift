//
//  QuestionStructure.swift
//  InterviewRecorder
//
//  Created by Shawn Fernando on 3/12/19.
//  Copyright © 2019 Shawn Fernando. All rights reserved.
//

import Foundation

struct Question{
    
    let type:String?
    let question:String?
    let min:Int?
    let max:Int?
    let multipleChoiceAnswers:[String]?
    let questionNumber:Int?
    
//    init(type: self.type = nil){
//        if type == multipleChoiceAnswerOption && multipleChoiceAnswers == nil{
//            print("There were no multiple choice answer options")
//
//        }
//        if type == numericalAnswerOption && (max == nil || min == nil){
//            print("min or max was nil")
//
//        }
//    }
    
}

struct Questionnaire{
    var questionArray: [Question]
}

func validateQuestion(question:Question) -> Bool{
    if question.question == nil || question.question == ""{
        print ("there was no Question")
        return false
    }
    if question.type == nil {
        print ("there was no question type")
        return false
    }
    if question.type == multipleChoiceAnswerOption {
        if question.multipleChoiceAnswers == [] {
            print ("no multiple choice answers")
            return false
        }
    }
    if question.type == numericalAnswerOption {
        if question.min == nil || question.max == nil {
            return false
        }
        
        if question.min != nil && question.max != nil {
            if question.min! >= question.max! {
                print ("min was above max")
                return false
            }
//            if question.min! == "" {
//                return false
//            }
//            if question.max! == "" {
//                return false
//            }
            
        }
    }
    return true
}
