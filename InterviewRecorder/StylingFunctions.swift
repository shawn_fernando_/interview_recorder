//
//  StylingFunctions.swift
//  NoStoryboard4
//
//  Created by Shawn Fernando on 1/26/19.
//  Copyright © 2019 Shawn Fernando. All rights reserved.
//

import Foundation
import UIKit

func singleColorLabel(UIColor: UIColor, text: String = "") -> UILabel{
    let returnLabel = UILabel()
    returnLabel.backgroundColor = UIColor
    returnLabel.text = text
    return returnLabel
}

func buttonWithTitle(title:String, color:UIColor = .gray) -> UIButton{
    let newButton:UIButton = UIButton()
    newButton.backgroundColor = color
    newButton.setTitle(title, for: .normal)
    return newButton
}


func whiteTextFieldWithPlaceholder(placeholder:String) -> UITextField{
    let newTextField:UITextField = UITextField()
    newTextField.placeholder = placeholder
    newTextField.backgroundColor = .white
    return newTextField
}

func whiteTextFieldWithBorderAndPlaceholder(placeholder:String) -> UITextField{
    let newTextField:UITextField = UITextField()
    newTextField.placeholder = placeholder
    newTextField.backgroundColor = .white
    addBorder(view: newTextField)
    return newTextField
}



func grayButtonWithBorderAndTitle(title:String) -> UIButton{
    let newButton:UIButton = UIButton()
    newButton.setTitle(title, for: .normal)
    newButton.backgroundColor = .gray
    addBorder(view: newButton)
    return newButton
}

func grayLabelWithStraightBorderAndTitle(title:String) -> UILabel{
    let newLabel:UILabel = UILabel()
    newLabel.text = title
    newLabel.backgroundColor = .gray
    newLabel.textAlignment = .center
    addBorder(view: newLabel, cornerRadius: 0.0)
    return newLabel
}

func justUIButtons(array:[AnyObject]) -> [UIButton]{
    var returnArray:[UIButton] = []
    for item in array{
        if item is UIButton{
            returnArray.append(item as! UIButton)
        }
    }
    return returnArray
}
    

func setBackgroundColorForViewArray(color:UIColor, array:[UIView]){
    for item in array{
        item.backgroundColor = color
    }

}

func setTargetForButton(selector: Selector, button:UIButton) -> UIButton{
    button.addTarget(button, action: selector, for: .touchUpInside)
    return button
    }

