//
//  NameOrEditQuestionnaireVC.swift
//  InterviewRecorder
//
//  Created by Shawn Fernando on 3/1/19.
//  Copyright © 2019 Shawn Fernando. All rights reserved.
//

import Foundation
import UIKit

class NameOrEditVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    var newQuestionnaireTextField = whiteTextFieldWithPlaceholder(placeholder: "Name a new Questionnaire")
    var newQuestionnaireStack = UIStackView()
    var newQuestionnaireAddButton = buttonWithTitle(title: "➕")
    var testLabel = singleColorLabel(UIColor: .blue)
    var testLabel2 = singleColorLabel(UIColor: .gray)
    var testLabel3 = singleColorLabel(UIColor: .green)
    var contentView = UIView()
    var scrollView = ControlContainableScrollView()
    var existingQuestionnairesLabel = singleColorLabel(UIColor: .gray, text: "Edit Existing Questionnaire")
    var existingQuestionnairesScrollView =  UIScrollView()
    var existingQuestionnairesTable = UITableView()
    var mainStack = UIStackView()
    let existingQuestionnaireNamesArray:[String] = ["Intake", "Something", "Something Else", "A Fourth", "Fifth", "And sixth"]
    let cellReuseIdentifier = "Cell"
    let testLabel4 = singleColorLabel(UIColor: .purple)
    
    override func viewDidLoad() {
        //existingQuestionnairesTable.delegate = self
        //existingQuestionnairesTable.dataSource = self
        
        situateMainStack(stack: mainStack, reference: self.view)
        existingQuestionnairesTable.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        newQuestionnaireAddButton.backgroundColor = .gray
        fillHorizontalStack(elementsArray: [newQuestionnaireTextField, newQuestionnaireAddButton], stack: newQuestionnaireStack, proportionsArray: [0.2])
        fillVerticalStackNoHeights(elementsArray: [newQuestionnaireStack, existingQuestionnairesLabel, contentView], stack: mainStack)
        
        newQuestionnaireTextField.delegate = self
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: newQuestionnaireTextField.frame.height))
        newQuestionnaireTextField.leftView = paddingView
        newQuestionnaireTextField.leftViewMode = .always
        newQuestionnaireAddButton.addTarget(self, action: #selector(addQuestionnaireButtonPressed(_:)), for: .touchUpInside)
        
        let existingQuestionnairesLabelHeight = existingQuestionnairesLabel.heightAnchor.constraint(equalTo: newQuestionnaireStack.heightAnchor, multiplier: 0.5)
        existingQuestionnairesLabel.textAlignment = .center
        existingQuestionnairesLabelHeight.priority = UILayoutPriority(rawValue: 1000)
        existingQuestionnairesLabelHeight.isActive = true
        addBorder(view: newQuestionnaireTextField)
        addBorder(view: newQuestionnaireAddButton)
        addBorder(view: existingQuestionnairesLabel, cornerRadius: 0.0)
        
        contentView.backgroundColor = .purple
        let testButton = buttonWithTitle(title: "bug?")
        
        scrollView.backgroundColor = .gray
        //scrollView.delaysContentTouches = true
        //scrollView.canCancelContentTouches = false
        //scrollView.touchesShouldCancel(in: testButton)
        let contentViewHeight = contentView.heightAnchor.constraint(equalTo: newQuestionnaireStack.heightAnchor, multiplier: 4.0)
        contentViewHeight.isActive = true
        testButton.addTarget(self, action: #selector(testButtonPressed(_:)), for: .touchUpInside)
        let buttonArray = createButtonArrayFromStringArray(stringArray: existingQuestionnaireNamesArray)
        //print(buttonArray)
        //let testLabelArray = [testLabel, testLabel2, testLabel3, testLabel4, testButton, singleColorLabel(UIColor: .red)]
        //for item in testLabelArray{
        //    scrollView.touchesShouldCancel(in: item)
        //}
        fillVerticalScrollViewNoHeights(container: contentView, scroll: scrollView, elementsArray: buttonArray)
//
        for item in buttonArray{
            item.heightAnchor.constraint(equalToConstant: 100).isActive = true
            addBorder(view: item, cornerRadius:0.0)
            item.addTarget(self, action: #selector(testButtonPressed(_:)), for: .touchUpInside)
            
        }

       
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 30
    }
    
    @objc func testButtonPressed(_ sender:UIButton){
        print("Button Pressed")
    }
    
    @objc func addQuestionnaireButtonPressed(_ sender:UIButton){
        print("addQuestionnaire Button Pressed")

        if newQuestionnaireTextField.text == ""{
            // create the alert
            let alert = UIAlertController(title: "Name your Questionnaire", message: "Enter a name in the box above :)", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
        }
        else{
            print(newQuestionnaireTextField.text as Any)
            
            if validateTextFieldString(textString: newQuestionnaireTextField.text ?? "") == true{
                print("text Validated, Go to create VC")
                let vc = CreateQuestionnaireVC()
                vc.questionnaireTitle = newQuestionnaireTextField.text!
                //UserDefaults.standard.set(["Something":"AnotherThing"], forKey: newQuestionnaireTextField.text!)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    
    func validateTextFieldString(textString:String) -> Bool{
        print("Validating Text")
        return true
    }
    
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return existingQuestionnaireNamesArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = existingQuestionnairesTable.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell? ?? UITableViewCell()
        // set the text from the data model
        cell.textLabel?.text = existingQuestionnaireNamesArray[indexPath.row]
        cell.backgroundColor = .white
        //cell.heightAnchor.constraint(equalTo: createNewQuestionnaireButton.heightAnchor, multiplier: 1.0)
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
}
        
        



