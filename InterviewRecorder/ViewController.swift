//
//  ViewController.swift
//  InterviewRecorder
//
//  Created by Shawn Fernando on 2/11/19.
//  Copyright © 2019 SunnyBear Software. All rights reserved.
//

import UIKit


class ViewController: UIViewController{

    let mainStack = UIStackView()
    override func viewDidLoad() {
        let createButton = buttonWithTitle(title: "Create a questionnaire")
        let runButton = buttonWithTitle(title: "Run a questionnaire")
        createButton.addTarget(self, action: #selector(createButtonTapped(_:)), for: .touchUpInside)
        runButton.addTarget(self, action: #selector(runButtonTapped(_:)), for: .touchUpInside)

        createButton.backgroundColor = .gray
        runButton.backgroundColor = .gray
        let safeArea = view.safeAreaLayoutGuide
        mainStack.distribution = .fillProportionally
        addSubviewToReference(subview: mainStack, reference: self.view)
        useAutoLayoutForView(view: mainStack)
        
        setAnchorsToLayoutGuide(view: mainStack, guide: safeArea)
        
        fillVerticalStack(elementsArray: [createButton, runButton], stack: mainStack)
        addBorder(view: createButton)
        addBorder(view: runButton)
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @objc func createButtonTapped(_ sender:UIButton){
        print("hit it")
        let vc = NameOrEditVC()
        //vc.catcher = "caught ya"
        print (self.navigationController as Any)
        self.navigationController?.pushViewController(vc, animated: true)
        
    
    }

    @objc func runButtonTapped(_ sender:UIButton){
        print("hit it")
        let vc = RunQuestionnaireVC()
        vc.catcher = "caught ya"
        print (self.navigationController as Any)
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

}

